
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Colours
#define COCOAPODS_POD_AVAILABLE_Colours
#define COCOAPODS_VERSION_MAJOR_Colours 5
#define COCOAPODS_VERSION_MINOR_Colours 6
#define COCOAPODS_VERSION_PATCH_Colours 1

// MCSwipeTableViewCell
#define COCOAPODS_POD_AVAILABLE_MCSwipeTableViewCell
#define COCOAPODS_VERSION_MAJOR_MCSwipeTableViewCell 2
#define COCOAPODS_VERSION_MINOR_MCSwipeTableViewCell 1
#define COCOAPODS_VERSION_PATCH_MCSwipeTableViewCell 2

// MagicalRecord
#define COCOAPODS_POD_AVAILABLE_MagicalRecord
#define COCOAPODS_VERSION_MAJOR_MagicalRecord 2
#define COCOAPODS_VERSION_MINOR_MagicalRecord 2
#define COCOAPODS_VERSION_PATCH_MagicalRecord 0

// MagicalRecord/Core
#define COCOAPODS_POD_AVAILABLE_MagicalRecord_Core
#define COCOAPODS_VERSION_MAJOR_MagicalRecord_Core 2
#define COCOAPODS_VERSION_MINOR_MagicalRecord_Core 2
#define COCOAPODS_VERSION_PATCH_MagicalRecord_Core 0

// SCLAlertView-Objective-C
#define COCOAPODS_POD_AVAILABLE_SCLAlertView_Objective_C
#define COCOAPODS_VERSION_MAJOR_SCLAlertView_Objective_C 0
#define COCOAPODS_VERSION_MINOR_SCLAlertView_Objective_C 5
#define COCOAPODS_VERSION_PATCH_SCLAlertView_Objective_C 5

