//
//  TDListCell.swift
//  ToDo
//
//  Created by Vedran Burojevic on 3/1/15.
//  Copyright (c) 2015 1337code. All rights reserved.
//

import UIKit

class TDTaskCell: MCSwipeTableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.defaultColor = UIColor.groupTableViewBackgroundColor()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
