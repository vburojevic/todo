//
//  Extensions.swift
//  
//
//  Created by Vedran Burojevic on 3/1/15.
//
//

import UIKit

// MARK: UIImageView

extension UIImageView {
    class func imageViewWithImageNamed(imageName: String) -> UIImageView {
        let image = UIImage(named: imageName) as UIImage!
        let imageView = UIImageView(image: image)
        imageView.contentMode = .Center
        
        return imageView
    }
}

// MARK: UIImage

extension UIImage {
    func imageWithColor(tintColor: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        
        let context = UIGraphicsGetCurrentContext() as CGContextRef
        CGContextTranslateCTM(context, 0, self.size.height)
        CGContextScaleCTM(context, 1.0, -1.0);
        CGContextSetBlendMode(context, kCGBlendModeNormal)
        
        let rect = CGRectMake(0, 0, self.size.width, self.size.height) as CGRect
        CGContextClipToMask(context, rect, self.CGImage)
        tintColor.setFill()
        CGContextFillRect(context, rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext() as UIImage
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
