//
//  CoreDataManager.swift
//  ToDo
//
//  Created by Vedran Burojevic on 3/1/15.
//  Copyright (c) 2015 1337code. All rights reserved.
//

import UIKit

let kUserDefaultsCurrentMaxTaskID = "currentMaxTaskID"

class CoreDataManager: NSObject {
    
    class func taskWithID(taskID: NSNumber) -> Task {
        let predicate = NSPredicate(format: "taskID == \(taskID)")
        let task: Task = Task.MR_findFirstWithPredicate(predicate) as Task
        
        return task
    }
    
    class func createTaskWithName(name: String, details: String, completion: ((success: Bool, error: NSError!) -> Void)?) {
        MagicalRecord.saveWithBlock({ (localContext) -> Void in
            
            let newTask = Task.MR_createInContext(localContext) as Task
            
            newTask.taskID = self.nextTaskID()
            newTask.name = name
            newTask.details = details
            newTask.createdAt = NSDate()
            
            }, completion: { (success, error) -> Void in
                if let completionBlock = completion {
                    completionBlock(success: success, error: error)
                }
        })
    }
    
    class func editTask(task: Task, newName: String, newDetails: String, completion: ((success: Bool, error: NSError!) -> Void)?) {
        MagicalRecord.saveWithBlock({ (localContext) -> Void in
            
            let existingTask = task.MR_inContext(localContext) as Task
            
            existingTask.name = newName
            existingTask.details = newDetails
            
            }, completion: { (success, error) -> Void in
                if let completionBlock = completion {
                    completionBlock(success: success, error: error)
                }
        })
    }
    
    class func completeTask(task: Task, completion: ((success: Bool, error: NSError!) -> Void)?) {
        MagicalRecord.saveWithBlock({ (localContext) -> Void in
            
            let localTask = task.MR_inContext(localContext) as Task
            localTask.completed = NSNumber(bool: true)
            localTask.completedAt = NSDate()
            
            }, completion: { (success, error) -> Void in
                if let completionBlock = completion {
                    completionBlock(success: success, error: error)
                }
        })
    }
    
    class func incompleteTask(task: Task, completion: ((success: Bool, error: NSError!) -> Void)?) {
        MagicalRecord.saveWithBlock({ (localContext) -> Void in
            
            let localTask = task.MR_inContext(localContext) as Task
            localTask.completed = NSNumber(bool: false)
            
            }, completion: { (success, error) -> Void in
                if let completionBlock = completion {
                    completionBlock(success: success, error: error)
                }
        })
    }
    
    class func deleteTask(task: Task, completion: ((success: Bool, error: NSError!) -> Void)?) {
        MagicalRecord.saveWithBlock({ (localContext) -> Void in
            
            let localTask = task.MR_inContext(localContext) as Task
            localTask.MR_deleteInContext(localContext)
            
            }, completion: { (success, error) -> Void in
                if let completionBlock = completion {
                    completionBlock(success: success, error: error)
                }
        })
    }
    
    class func loadTasks() -> [Task]! {
        let allTasks = Task.MR_findAll() as [Task]!
        
        return allTasks
    }
    
    class func lastTaskID() -> NSNumber {
        if let lastID = NSUserDefaults.standardUserDefaults().objectForKey(kUserDefaultsCurrentMaxTaskID) as? NSNumber {
            return lastID
        } else {
            return NSNumber(int: 0)
        }
    }
    
    class func nextTaskID() -> NSNumber {
        let currentMaxTaskID = self.lastTaskID()
        
        let nextTaskID = NSNumber(int: (currentMaxTaskID.intValue + 1))
        
        NSUserDefaults.standardUserDefaults().setObject(nextTaskID, forKey: kUserDefaultsCurrentMaxTaskID)
        
        return nextTaskID
    }
}
