//
//  Task.swift
//  ToDo
//
//  Created by Vedran Burojevic on 3/1/15.
//  Copyright (c) 2015 1337code. All rights reserved.
//

import Foundation
import CoreData

@objc(Task)
class Task: NSManagedObject {

    @NSManaged var completedAt: NSDate
    @NSManaged var createdAt: NSDate
    @NSManaged var details: String
    @NSManaged var name: String
    @NSManaged var taskID: NSNumber
    @NSManaged var completed: NSNumber

}
