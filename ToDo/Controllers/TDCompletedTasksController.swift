//
//  TDCompletedTasksController.swift
//  ToDo
//
//  Created by Vedran Burojevic on 3/1/15.
//  Copyright (c) 2015 1337code. All rights reserved.
//

import UIKit

class TDCompletedTasksController: UITableViewController, NSFetchedResultsControllerDelegate {
    
    // MARK: Properties
    
    var _fetchedResultsController: NSFetchedResultsController? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        performFetchRequest()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        configureAppearance()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Methods
    
    func configureAppearance() {
        self.tableView.backgroundColor = UIColor.snowColor()
        self.navigationController?.navigationBar.barTintColor = UIColor.emeraldColor()
        self.tabBarController?.tabBar.barTintColor = UIColor.emeraldColor()
    }

    // MARK: UITableViewDelegate
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.fetchedResultsController.sections?.count ?? 0
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var sectionInfo = self.fetchedResultsController.sections![section] as? NSFetchedResultsSectionInfo
        
        if let sectionInformation = sectionInfo {
            return sectionInfo!.numberOfObjects
        } else {
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("TaskCell", forIndexPath: indexPath) as TDTaskCell
        
        if let task = self.fetchedResultsController.objectAtIndexPath(indexPath) as? Task {
            configureCell(cell, indexPath: indexPath, task: task)
        }
        
        return cell
    }
    
    func configureCell(cell: TDTaskCell, indexPath: NSIndexPath, task: Task) {
        
        cell.titleLabel.text = task.name
        cell.detailsLabel.text = task.details
        
        let incompleteView = UIImageView.imageViewWithImageNamed("undo")
        let deleteView = UIImageView.imageViewWithImageNamed("trash")
        
        cell.setSwipeGestureWithView(incompleteView, color: UIColor.indigoColor(), mode: MCSwipeTableViewCellMode.Exit, state: MCSwipeTableViewCellState.State1) { (cell: MCSwipeTableViewCell!, state: MCSwipeTableViewCellState, mode: MCSwipeTableViewCellMode) -> Void in
            // Incomplete
            CoreDataManager.incompleteTask(task, completion: nil)
        }
        
        cell.setSwipeGestureWithView(deleteView, color: UIColor.watermelonColor(), mode: MCSwipeTableViewCellMode.Switch, state: MCSwipeTableViewCellState.State3) { (cell: MCSwipeTableViewCell!, state: MCSwipeTableViewCellState, mode: MCSwipeTableViewCellMode) -> Void in
            // Delete
            let alert = SCLAlertView()
            alert.backgroundType = SCLAlertViewBackground.Blur
            alert.shouldDismissOnTapOutside = true
            alert.showAnimationType = SCLAlertViewShowAnimation.SlideInFromTop
            alert.hideAnimationType = SCLAlertViewHideAnimation.SlideOutToBottom
            
            alert.addButton("Yes", actionBlock: { () -> Void in
                CoreDataManager.deleteTask(task, completion: nil)
            })
            
            alert.addButton("No", actionBlock: { () -> Void in
                cell.swipeToOriginWithCompletion(nil)
            })
            
            alert.showTitle(self.tabBarController, title: "Delete task", subTitle: "Are you sure you want to delete task \"\(task.name)\"", style: SCLAlertViewStyle.Notice, closeButtonTitle: nil, duration: 0.0)
        }
    }
    
    // MARK: - Fetched results controllers
    
    func performFetchRequest() {
        var error: NSError?
        self.fetchedResultsController.performFetch(&error)
        
        if let fetchError = error {
            println("ERROR WHILE FETCHING TASKS: \(error)")
        }
    }
    
    var fetchedResultsController: NSFetchedResultsController {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest = self.defaultFetchRequest()
        
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: NSManagedObjectContext.MR_defaultContext()!, sectionNameKeyPath: nil, cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        var error: NSError? = nil
        if !_fetchedResultsController!.performFetch(&error) {
            println(error)
        }
        
        return _fetchedResultsController!
    }
    
    func defaultFetchRequest() -> NSFetchRequest {
        let fetchRequest = NSFetchRequest()
        // Edit the entity name as appropriate.
        let entity = NSEntityDescription.entityForName("Task", inManagedObjectContext: NSManagedObjectContext.MR_defaultContext()!)
        
        fetchRequest.entity = entity
        
        // Predicate
        let predicate = NSPredicate(format: "completed == true")
        fetchRequest.predicate = predicate
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: "completedAt", ascending: true)
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        return fetchRequest
    }
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        tableView.beginUpdates()
    }
    
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        
        var tableView : UITableView = self.tableView
        
        switch type {
        case .Insert:
            tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
        case .Delete:
            tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
        default:
            return
        }
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        
        switch type {
        case .Insert:
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Top)
        case .Delete:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Top)
        case .Update:
            if let cell = self.tableView.cellForRowAtIndexPath(indexPath!) as? TDTaskCell {
                let task = anObject as Task
                configureCell(cell, indexPath: indexPath!, task: task)
            }
        case .Move:
            self.tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Top)
            self.tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Top)
        default:
            return
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.endUpdates()
    }
    
}
